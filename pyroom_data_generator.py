import numpy as np
import pyroomacoustics as pra
import csv
import os
from scipy.io import wavfile
from scipy import signal
from sklearn import preprocessing
import parameters

class DataGenerator(object):

    """
    Data Generator class for creating synthetic datasets of room-recorded sound pieces with corresponding labels.
    It uses DCase2016 dataset as sound samples and PyRoomAcoustics library for applying room impulse response.

    --------------------------------------------------------------------------------------------------------------------

    WARNING! Make sure you specified input_dir while initializing instance, or make sure default
             path exists - this class doesn't check that.
    WARNING! Make sure input directory contains only .wav files!
    WARNING! Make sure all input .wav files are sampled with the same frequency!

    It is possible to set NoAS and event count beyond what is possible to fit in given length. This class doesn't deal
    with that and goes in infinite loop. However, after some time, warning shall be printed which shall suggest to kill
    process and rerun it with adjusted parameters. Keep in mind that this warning depends solemnly on number of guesses
    in probabilistic algorithm, which means that it is technically possible it will find solution given enough time.
    """

    def __init__( self, input_dir=None, output_dir=None, rm_previous_data=False):
        """
        Initialize object

        :param input_dir: (str)
                    path which specifies directory containing all DCase2016 training samples.
                    WARNING: This directory should contain only .wav files!!! DataGenerator does not check it,
                             yet depends on it.
                             If not given, this class expects following default path to be present (errors otherwise!!):
                                project_root/rawdata/dcase2016_task2_train
                             Above path becomes input_directory
                    WARNING: All input .wav files in this directory should have the same sampling frequency!!
                             (labels corruption otherwise)
        :param output_dir: (str)
                    path which specifies directory for output files
                    If not given, this class assumes following default path:
                        project_root/data/synthetic
                    As output_directory
        :param rm_previous_data: (boolean)
                    If set to true, all data in output directory will be erased
        """
        if input_dir is not None:
            self._input_dir = input_dir
        else:
            self._input_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "rawdata", "dcase2016_task2_train")
        if output_dir is not None:
            self._output_dir = output_dir
        else:
            if not os.path.exists(os.path.join(os.path.dirname(os.path.realpath(__file__)), "data", "synthetic")):
                os.makedirs(os.path.join(os.path.dirname(os.path.realpath(__file__)), "data", "synthetic"))
            self._output_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data", "synthetic")
        if not os.path.exists(os.path.join(self._output_dir,"wav")):
            os.makedirs(os.path.join(self._output_dir,"wav"))
        if not os.path.exists(os.path.join(self._output_dir, "labels")):
            os.makedirs(os.path.join(self._output_dir, "labels"))
        if not os.path.exists(os.path.join(self._output_dir, "spectros")):
            os.makedirs(os.path.join(self._output_dir, "spectros"))
        _, _, self._filenames = next(os.walk(self._input_dir), (None, None, []))
        self.count = 0
        self._guardian = 0

        self._params = parameters.get_params()

        #Spectro
        self._nfft = self._params["nfft"]
        self._win_len = self._nfft
        self._hop_len = self._nfft/2
        self._hop_len_s = self._nfft / 2.0 / 44100
        self._audio_max_len_samples = 30 * 44100
        self._max_frames = int(np.ceil((self._audio_max_len_samples - self._win_len) / float(self._hop_len)))

        self.sound_classes = {'clearthroat': 0, 'cough': 1, 'doorslam': 2, 'drawer': 3, 'keyboard': 4, 'keysDrop': 5,
                              'knock': 6, 'laughter': 7, 'pageturn': 8, 'phone': 9, 'speech': 10}
        self._gen_labels_file = os.path.join(self._output_dir, "labels.csv")
        self.__create_general_labels_file()
        if rm_previous_data is True:
            self.remove_all_synthetic_data()

    def __create_general_labels_file(self):
        """
        Creates in output directory (if it not exists) .csv file for labels, named "labels.csv". Automatically writes
        header row containing column names.
        :return: None
        """
        try:
            f = open(self._gen_labels_file, 'r')
            f.close()
        except FileNotFoundError:
            f = open(self._gen_labels_file, 'w+')
            header = np.array(["no", "room_lenght", "room_width", "mic_pos_x", "mic_pos_y", "mic_number", "mic_radius",
                               "mic_rotation", "events_count", "noas", "noise", "fs", "wav_length"])
            f_writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            f_writer.writerow(header)
            f.close()

    def __create_events_label_file(self):
        """
        Creates in output_directory/labels .csv file (if it not exists) for labels, named "labels.csv". Automatically
        writes header row containing column names.
        :return: None
        """
        while True:
            file = os.path.join(self._output_dir, "labels", str(self.count) + ".csv")
            try:
                f = open(file, 'r')
                f.close()
                self.count += 1
            except FileNotFoundError:
                f = open(file, "w+")
                header = np.array(["class", "start", "timespan", "doa"])
                f_writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                f_writer.writerow(header)
                f.close()
                break

    def __save_signal(self, signal):
        """
        Saves computed microphone response in separate .wav file. Name of the .wav file is "x.wav" where x is the next
        available number. Also saving spectrograms (see __save_spectrograms)
        :param signal: (pyroomacoustics.MicrophoneArray) object containing signals to save.
        :return: None
        """
        while True:
            file = os.path.join(self._output_dir, "wav", str(self.count) + ".wav")
            try:
                f = open(file, 'r')
                f.close()
                self.count += 1
            except FileNotFoundError:
                f = open(file, "w+")
                f.close()
                signal.to_wav(file,norm=True)

                fs, audio = wavfile.read(file)
                self.__save_spectrograms(audio)
                break

    def __save_labels(self, labels, type):
        """
        Appends generated current labels to appropriate file, specified by 'type' parameter
        :param labels: Labels as np.array
        :param type: set to 'general' or 'event' based on what labels are you saving
        :return: None
        """
        if type is "general":
            with open(self._gen_labels_file, mode='a') as f:
                f_writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                f_writer.writerow(labels)
            return
        if type is "event":
            with open(os.path.join(self._output_dir, "labels", str(self.count) + ".csv"), mode='a') as f:
                f_writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                f_writer.writerow(labels)
            return
        print("type argument must be set to 'general or 'event'! Nothing happened.")

    def __save_spectrograms(self, X):
        """
        Saves spectrogram of each given audio channel as a numpy.array in seperate files in output_dir/spectros
        Name of each file is X.npy where X is a number corresponding to wav & csv files in wav/ and labels/
        This function for some reasons 'concatenate' multi-channel audio, so information about number of channels is
        not being remembered here. If number of channels is known, original per-channel signals can be restored by
        reshaping spectrogram into (spec.shape[0], spec.shape[1] / nb_channels, nb_channels)
        Spectrograms are saved in form of concatenated arrays of magnitude & phase.
        :param X: (np.array)
                audio to extract spectrogram from
        :return: None
        """
        spectro_scaler = preprocessing.StandardScaler()
        while True:
            file = os.path.join(self._output_dir, "spectros", str(self.count) + ".npy")
            try:
                f = open(file, 'r')
                f.close()
                self.count += 1
            except FileNotFoundError:
                _nb_ch = X.shape[1]
                hann_win = np.repeat(np.hanning(self._win_len)[np.newaxis].T, _nb_ch, 1)
                nb_bins = self._nfft / 2
                spectrogram = np.zeros((self._max_frames, int(nb_bins), _nb_ch), dtype=complex)
                for ind in range(self._max_frames):
                    start_ind = ind * int(self._hop_len)
                    aud_frame = X[start_ind + np.arange(0, self._win_len), :] * hann_win
                    spectrogram[ind] = np.fft.fft(aud_frame, n=self._nfft, axis=0, norm='ortho')[:int(nb_bins), :]
                spectrogram = spectrogram.reshape(self._max_frames, -1)
                spectrogram = spectro_scaler.fit_transform(np.concatenate((np.abs(spectrogram), np.angle(spectrogram)),
                                                                          axis=1))
                np.save(file, spectrogram)
                break

    def remove_all_synthetic_data(self):
        """
        Removes all files in 'synthetic' directory and resets "labels.csv"
        :return: None
        """
        for root, _, rm_files in os.walk(self._output_dir):
            for file in rm_files:
                os.remove(os.path.join(root, file))
        self.__create_general_labels_file()

    def __extend_sample(self, sample, start, length, noise=None):
        """
        extending each sound sample to fixed length, adding random noise with given max amplitude (can be 0)
        :param sample: (np.array)
                audio to be extended
        :param start: (int)
                start point (in frames) for audio
        :param length: (int)
                length for audio to be extended to.
        :param noise: (int)
                specifies max amplitude of random noise
        :return: extended signal as an np.array
        """
        ret_signal = np.random.randint(-1 * noise, noise + 1, length)
        ret_signal[start : start + len(sample)] = sample + ret_signal[start : start + len(sample)]
        return ret_signal

    def __check_for_noas(self,start,length,noas):
        '''
        Checks if it is possible to add another active source in time from 'start' to 'start + length'
        :param start:  (int)
                    Number of sample when source wants to start emitting sound
        :param length:  (int)
                    Length of source signal given in number of samples
        :param noas:  (int)
        :return: Boolean value indicting whether it is possible or not to add another source in given timespan
        '''
        for i in self._guardian[start : start + length]:
            if i + 1 > noas:
                return False
        return True

    def generate_data(self, room_length=None, room_width=None, rand_seed=None, mics_number=None, mics_radius=0.2,
                      output_length=None, number_of_active_sources=None, number_of_events=None, noise=0, fs=44100,
                      iterations=1, shuffle_files=True):
        '''
        This method actually generates data. For each file it sets up the scene (room, source, microphone), make labels
        and computes corresponding signal. Signal and labels are saved in output_dir
        First column (no) in 'labels.csv' file specifies to which .wav and .csv the row refers to, for example:
        if 1st column in 3rd row is '3.0' it means that corresponding .wav and .csv are named '3.wav' and '3.csv'

        :param room_length: (int)
                    If given, sets room length parameter. Otherwise randomized from 4 to 10 inclusive.
        :param room_width: (int)
                    If given, sets room width parameter. Otherwise randomized from 4 to 10 inclusive.
        :param rand_seed: (int)
                    Sets np.random.seed for reproducibility. WARNING: Globally.
        :param mics_number: (int)
                    Specifies number of microphones used in circular_array
        :param mics_radius: (float)
                    Specifies radius of microphone circular array
        :param output_length: (int)
                    Specifies length of output .wav files in seconds
        :param number_of_active_sources: (int)
                    Specifies how many sounds may be played at the same time. Default value = 1 (no overlapping)
        :param number_of_events: (int)
                    Specifies how many events will appear in output .wav file(s). If unset, it will be randomized with
                    each .wav file
        :param noise: (int)
                    Special factor which defines background noise of .wav file(s). Defaults 0 means no noise
                    (Note that this value is not SNR in decibels - it simply defines max amplitude of random noise)
        :param fs: (int)
                    Sampling frequency of output file.
                    WARNING! Features linked with this parameter are not yet fully implemented! Please, for the time
                             being always set it to the input .wav files sampling frequency!
        :param iterations: (int)
                    Specifies number of output files produced
        :param shuffle_files: (boolean)
                    If set to True input file list are reshuffled with each iteration (1st one included)
        :return: None
        '''
        # len = 1 323 000
        # fs = 44100
        if mics_number is None:
            mics_number = self._params['nb_channels']

        if rand_seed is not None:
            np.random.seed(rand_seed)
        if room_length is not None:
            room_l = room_length
        if room_width is not None:
            room_w = room_width
        if number_of_events is not None:
            n_o_events = number_of_events
        if number_of_active_sources is not None:
            noas = number_of_active_sources
        if output_length is None:
            output_length = self._params['audio_length']
        output_l = output_length * fs

        for _ in range(iterations):
            self.count += 1
            self._guardian = np.zeros(output_l)

            if shuffle_files is True:
                np.random.shuffle(self._filenames)
            if number_of_active_sources is None:
                noas = np.random.randint(1, 4)
            if number_of_events is None:
                n_o_events = np.random.randint(noas * 5, int(noas * (output_length / 3)))
            if room_length is None:
                room_l = np.random.randint(4, 11)
            if room_width is None:
                room_w = np.random.randint(4, 11)
            room = pra.ShoeBox([room_l, room_w], fs=fs)

            micro_pos_x = np.random.uniform(mics_radius, room_l - mics_radius)
            micro_pos_y = np.random.uniform(mics_radius, room_w - mics_radius)
            mic_rotation = np.random.uniform(-1 * np.pi, np.pi)
            mic_array = pra.circular_2D_array(center=[micro_pos_x, micro_pos_y], M=mics_number, phi0=mic_rotation,
                                              radius=mics_radius)
            room.add_microphone_array(pra.MicrophoneArray(mic_array, room.fs))

            self.__create_events_label_file()
            general_labels = np.array([self.count, room_l, room_w, micro_pos_x, micro_pos_y, mics_number, mics_radius,
                                       mic_rotation, n_o_events, noas, noise, fs, output_length])
            self.__save_labels(general_labels,'general')

            for file in np.random.choice(self._filenames,n_o_events):
                _, audio = wavfile.read(os.path.join(self._input_dir, file))
                time_span = len(audio)

                flag = False
                kill_condition = 0
                checkpoint = 10000
                while not flag:
                    timestamp = np.random.randint(0,output_l - len(audio))
                    flag = self.__check_for_noas(timestamp,time_span,noas)
                    kill_condition += 1
                    if kill_condition == checkpoint:
                        print("I have randomized over %s samples and had no success in satisfying NoAS and "
                              "events number value. This task might take a very long time and might be even impossible "
                              "to do. Please, consider killing this process and rerun it with higher value of"
                              "number_of_active_sources or lower value of number_of_events." % checkpoint, flush=True)
                        checkpoint += 10000
                self._guardian[timestamp:timestamp + time_span] = self._guardian[timestamp:timestamp + time_span] + 1

                audio = self.__extend_sample(audio, timestamp, output_l, noise=noise)
                source_pos_x = np.random.uniform(0.0, room_l)
                source_pos_y = np.random.uniform(0.0, room_w)
                room.add_source([source_pos_x, source_pos_y], signal=audio)

                doa_label = np.arctan2(source_pos_y - micro_pos_y, source_pos_x - micro_pos_x)
                doa_label = doa_label - mic_rotation

                timestamp = (timestamp / output_l) * output_length
                time_span = (time_span / output_l) * output_length
                class_label = self.sound_classes[file[:len(file)-7]]
                event_labels = np.array([class_label, timestamp, time_span, doa_label])
                self.__save_labels(event_labels, 'event')

            room.simulate()
            self.__save_signal(room.mic_array)
            print("Finished %s.wav!" % self.count)
