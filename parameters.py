def get_params():
    params = dict(
        # Params for audio generation that are important and should be consistent during whole process
        audio_length=30,
        nb_channels=8,
        nb_classes=11,

        # Params for spectrogram generation
        nfft = 512,
        sequence_length=512,

        # Params for dnn model & learning
        batch_size=4,
        cnn_size=[32, 64, 128],  # CNN conv layers, length of list = nb of CNN layers, value = nb of filters per layer
        conv_filters_size=(3, 3),
        pool_size=[8, 8, 2],  # CNN pooling, length of list = nb of CNN layers, value = pooling per layer
        rnn_size=[128, 128],  # RNN contents, length of list = number of layers, list value = number of nodes
        fnn_size=[128],  # FNN contents, length of list = number of layers, list value = number of nodes
        loss_weights=[1., 50.],  # [sed, doa] weight for scaling the DNN outputs
        dropout=0.5,
        leaky_relu=0.2   # May not be used if relu activasion
    )

    # Tests for integrity
    if len(params['cnn_size']) != len(params['pool_size']):
        print("ERROR in parameters. pool_size should match in length cnn_size!")

    return params
