import pyroom_data_generator as pdg
import dnn_model as model

dg = pdg.DataGenerator(rm_previous_data=True)
dg.generate_data(iterations=100)

dnn = model.DnnModel(print_sum=True)
# dnn.load_model()
dnn.train(10)

# visualize 5 random samples:
for i in range(5):
    spect, _, s, d = dnn.get_next_batch()
    lab = [s, d]
    out = dnn.predict(spect)
    dnn.visualize_batch(lab, out)


