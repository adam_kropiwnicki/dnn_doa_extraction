### dnn_doa_extraction is a project made by a Wroclaw University of Science and Technology student.
###### Project was initially supervised by SAMSUNG ELECTRONICS Polska.
***
#####Project include:
- data generator which creates training and test data sets from DCASE2016 sound samples (and applies to them syntetic room transformation via PyRoomAccoustics library) 
- DNN model built in Keras API
*** 
Parameters of training are being set in seperate parameters.py file
***
##### To do:
* Add evaluation methods
* Upload database of real room impulse responses
* Extend data generator to work with more sound samples
* Optimize DNN model