import tensorflow as tf
import numpy as np
import csv
import os
from keras.layers import Bidirectional, Conv2D, MaxPooling2D, Input, LeakyReLU
from keras.layers.core import Dense, Activation, Dropout, Reshape, Permute
from keras.layers.recurrent import GRU
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.layers.wrappers import TimeDistributed
from keras.optimizers import Adam
import keras
import matplotlib.pyplot as plt
import parameters


class DnnModel(object):
    """
    Wrapper class for keras dnn model and data fetcher.

    Warning: Make sure to have proper input_directory structure which means:
                It should have 'spectros' directory containing consistent .npy files (and ONLY .npy files)
                It should have 'labels' directory with corresponding label .csv files
                it should have 'labels.csv' with general audio informations
    """
    def __init__(self, input_dir=None, output_dir=None, split_data=None, print_sum=False):
        """
        Initialize instance. Parameters of dnn are loaded from parameters.py file.
        :param input_dir: (str)
                    Directory where this class is looking for spectrograms and labels. Currently should be set to
                    output_dir of pyroom_data_generator
        :param output_dir: (str)
                    specifies location of model saving files
        :param split_data: (float)
                    specifies train-to-all ratio for input dataset to be split into. For example, split_data = 0.8 will
                    split input data to 80% train and 20% test. If set to None (default value) data will not be split,
                    treating every data piece as training samples
        :param print_sum:  (bool)
                    specifies whether to print a summary of model. Defaults to False
        """

        if input_dir is not None:
            self._input_dir = input_dir
        else:
            self._input_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data", "synthetic")
        if output_dir is not None:
            self._output_dir = output_dir
        else:
            if not os.path.exists(os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")):
                os.makedirs(os.path.join(os.path.dirname(os.path.realpath(__file__)), "data"))
            self._output_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")
        _, _, filenames = next(os.walk(os.path.join(self._input_dir, "spectros")), (None, None, []))
        if split_data is not None:
            self._train_filenames = filenames[:int(len(filenames)*split_data)]
            self._test_filenames = filenames[int(len(filenames)*split_data):]
        else:
            self._train_filenames = filenames
            self._test_filenames = None

        self._params = parameters.get_params()

        self._count = 0
        self._epochs_done = 0

        self._model = self.__init_model()
        if print_sum is True:
            self._model.summary()

    def __init_model(self):
        """
        Instantiate backend keras dnn model
        :return:
        """
        data_out = [
            (self._params['batch_size'], self._params['sequence_length'], self._params['nb_classes']),
            (self._params['batch_size'], self._params['sequence_length'], self._params['nb_classes'])
        ]
        # Need to develop a way to compute feature length. For now, hardocded to 512, so dont change nfft
        data_in = [self._params['batch_size'], self._params['sequence_length'], 512, self._params['nb_channels']]

        spec_start = Input(shape=(data_in[-3], data_in[-2], data_in[-1]))
        spec_cnn = spec_start

        # this pooling layer has been added so the creator of this script doesnt run out of RAM.
        # it is really destructive layer and should be removed/commented out in final form (or with more than 8Gb RAM :D
        # spec_cnn = MaxPooling2D(pool_size=(1, 2))(spec_cnn)

        for i, conv in enumerate(self._params['cnn_size']):
            spec_cnn = Conv2D(filters=conv, kernel_size=self._params['conv_filters_size'],
                              padding='same')(spec_cnn)
            spec_cnn = BatchNormalization()(spec_cnn)

            # Normal ReLU is used in original SELDnet. Possible change to leakyReLU to prevent vanishing gradient
            #spec_cnn = Activation('relu')(spec_cnn)
            spec_cnn = LeakyReLU(self._params['leaky_relu'])(spec_cnn)

            spec_cnn = MaxPooling2D(pool_size=(1, self._params['pool_size'][i]))(spec_cnn)
            spec_cnn = Dropout(self._params['dropout'])(spec_cnn)

        spec_cnn = Permute((1, 3, 2))(spec_cnn)
        spec_rnn = Reshape((data_in[-3], -1))(spec_cnn)

        for nb_rnn_filt in self._params['rnn_size']:
            spec_rnn = Bidirectional(
                GRU(nb_rnn_filt, activation='tanh', dropout=self._params['dropout'],
                    recurrent_dropout=self._params['dropout'], return_sequences=True),
                merge_mode='mul'
            )(spec_rnn)

        doa = spec_rnn
        for nb_fnn_filt in self._params['fnn_size']:
            doa = TimeDistributed(Dense(nb_fnn_filt))(doa)
            doa = Dropout(self._params['dropout'])(doa)

        doa = TimeDistributed(Dense(data_out[1][-1]))(doa)
        doa = Activation('tanh', name='doa_out')(doa)

        # SED
        sed = spec_rnn
        for nb_fnn_filt in self._params['fnn_size']:
            sed = TimeDistributed(Dense(nb_fnn_filt))(sed)
            sed = Dropout(self._params['dropout'])(sed)

        sed = TimeDistributed(Dense(data_out[0][-1]))(sed)
        sed = Activation('sigmoid', name='sed_out')(sed)

        model = Model(inputs=spec_start, outputs=[sed, doa])
        model.compile(optimizer=Adam(), loss=['binary_crossentropy', 'mse'], loss_weights=self._params['loss_weights'])
        return model

    def train(self, epochs, save_freq=100):
        """
        Train model with batches fetched one by one for a specified number o epochs.
        Saves model to a default location every 'save_freq' epochs
        :param epochs: Number of epochs to be processed
        :param save_freq: Frequency of model savings
        :return:
        """
        self._epochs_done = 0
        self._count = 0
        save_count = 0
        while self._epochs_done != epochs:
            input_spec, _, sed, doa = self.get_next_batch()
            labels = [sed, doa]
            self._model.fit(input_spec, labels, self._params['batch_size'])
            print(self._epochs_done)
            save_count += 1
            if save_count == save_freq:
                self.save_model()
                save_count = 0

    def predict(self, spec):
        """
        Make a prediction with underlying model.
        :param spec: (np.array)
                Input data to make predictions on.
        :return: (np.array)
                Predictions that has been made
        """
        output = self._model.predict(spec)
        return output

    def __encode_labels(self, e_label, spec_length):
        """
        Encoding labels to form a matrix of time distributed one-hot vectors for sed & doa task
        :param e_label: np.array with labels loaded from x.csv file
        :param spec_length: (int)
                length of corresponding to e_label spectrogram in time frames (shape[0])
        :return: sed, doa -> labels ready to be fed to a dnn model
        """
        sed_label = np.zeros((spec_length, self._params['nb_classes']))
        doa_label = np.zeros((spec_length, self._params['nb_classes']))
        for i in range(e_label.shape[0]):
            start_p = int(e_label[i][1] * spec_length / self._params['audio_length'])
            end_p = int(e_label[i][1] * spec_length / self._params['audio_length']) + int(e_label[i][2] * spec_length / self._params['audio_length'])
            sed_label[start_p:end_p, int(e_label[i][0])] = 1
            doa_label[start_p:end_p, int(e_label[i][0])] = e_label[i][3]
        doa_label /= 2*np.pi
        return sed_label, doa_label

    def save_model(self, filename=None):
        """
        Save model to a file. Default location -> output_dir/current_model.h5
        :param filename: (str)
                filename for model to be saved. If not given default is used
        :return: None
        """
        if filename is None:
            self._model.save(os.path.join(self._output_dir, "current_model.h5"))
        else:
            self._model.save(os.path.join(self._output_dir, filename))

    def load_model(self, filename=None, print_sum=True):
        """
        Load model from a file. By default trying to locate a file in -> output_dir/current_model.h5
        :param filename: (str)
                filename to a file with model weights
        :param print_sum: (bool)
                Specifies whether to print summary of model after loading. Defaults to True
        :return: None
        """
        if filename is None:
            self._model = keras.models.load_model(os.path.join(self._output_dir, "current_model.h5"))
        else:
            self._model = keras.models.load_model(os.path.join(self._output_dir, filename))
        if print_sum is True:
            self._model.summary()

    def get_next_batch(self, train=True):
        """
        Grabbing next batch of data: Random, but unique in single epoch.
        Loaded data consist of:
                        spectrograms located in output_dir/spectros/
                        general data located in output_dir/labels.csv
                        labeling data located in output_dir/labels/
        This function reshapes spectrograms into channels and splits spectrograms into sequences, encode sed and doa
        labels into batches of 'one-hot vectors' and maintain number of epochs already done

        WARNING! Spliting into sequences is poorly coded. Almost always will result in discarding plenty of data,
                 lowering dataset potential.
                 To prevent it, set batch_size to be equal of int( spectrogram.shape[0] / sequence_length )

        :param train: (bool)
                    Specifies whether to grab a train batch or test batch. Default means train
        :return: 4 np.arrays -> spec, general_labels, sed, doa :
                    spec -> to be fed into dnn model as input. Shape = [batch_size, time_frames, features, channels]
                    general_labels -> labels containing information about how audio was produced. Shape = [batch_size, 13]
                    sed -> labels for sed task. One-hot encoded. Shape = [batch_size, time_frames, nb_classes]
                    doa -> labels for doa task. One-hot encoded. Shape = [batch_size, time_frames, nb_classes]
        """
        spectro = None
        general_labels = None
        sed_labels = None
        doa_labels = None
        batches_done = 0
        if train is True:
            filenames = self._train_filenames
        elif self._test_filenames is None:
            print("Error: Data has not been split to train/test sets when model was instantiated! Terminating...")
            return None, None, None, None
        else:
            filenames = self._test_filenames

        while batches_done != self._params['batch_size']:
            if self._count == 0:
                np.random.shuffle(filenames)

            spec = np.load(os.path.join(self._input_dir, "spectros", filenames[self._count]))
            name = filenames[self._count][:len(filenames[self._count])-4]
            g_label = None
            with open(os.path.join(self._input_dir, "labels.csv"), mode='r') as gen_labels:
                f_reader = csv.reader(gen_labels, delimiter=',')
                for row in f_reader:
                    if row[0] == name + ".0":
                        g_label = np.asarray(row, dtype=float)
            if g_label is None:
                raise Exception("No row in labels.csv matches file {}.npy".format(int(name)))

            try:
                f = open(os.path.join(self._input_dir, "labels", name + ".csv"), 'r')
                e_label = np.loadtxt(f, delimiter=',', skiprows=1)
                f.close()
            except FileNotFoundError:
                print("Error: File with matching event labels ({}.csv) has not been found. Terminating".format(int(name)))
                exit(1)

            sed_lab, doa_lab = self.__encode_labels(e_label, spec.shape[0])
            for j in range(int((spec.shape[0] - (spec.shape[0] % self._params['sequence_length']))
                               / self._params['sequence_length'])):
                start = j * self._params['sequence_length']
                end = (j+1) * self._params['sequence_length']
                spec_tmp = spec[start:end, :]
                s_label = sed_lab[start:end, :]
                d_label = doa_lab[start:end, :]

                g_label_tmp = np.expand_dims(g_label, axis=0)
                s_label_tmp = np.expand_dims(s_label, axis=0)
                d_label_tmp = np.expand_dims(d_label, axis=0)

                spec_tmp = np.reshape(spec_tmp, (1, self._params['sequence_length'],
                                                 int(spec.shape[1] / self._params['nb_channels']),
                                                 self._params['nb_channels']))

                if spectro is None:
                    spectro = spec_tmp
                else:
                    spectro = np.concatenate((spectro, spec_tmp), axis=0)
                if general_labels is None:
                    general_labels = g_label_tmp
                else:
                    general_labels = np.concatenate((general_labels, g_label_tmp), axis=0)
                if sed_labels is None:
                    sed_labels = s_label_tmp
                else:
                    sed_labels = np.concatenate((sed_labels, s_label_tmp), axis=0)
                if doa_labels is None:
                    doa_labels = d_label_tmp
                else:
                    doa_labels = np.concatenate((doa_labels, d_label_tmp), axis=0)
                batches_done += 1
                if batches_done == self._params['batch_size']:
                    break
            self._count += 1
            if self._count == len(filenames):
                self._count = 0
                self._epochs_done += 1

        return spectro, general_labels, sed_labels, doa_labels

    def visualize_batch(self, labels, predicted):
        """
        Visualize single batch with matplotlib.
        :param labels: (list of 2 numpy arrays (sed, doa))
                labels to be visualized
        :param predicted: (list of 2 numpy arrays (sed, doa))
                predicted output to be visualized. Can be set to None to visualize only one set.
        :return: None
        """
        plt.figure()
        names = [["sed - target", "doa - target"], ["sed - output", "doa - output"]]
        for i in range(2):
            x = np.arange(labels[i].shape[0] * labels[i].shape[1])
            y = None
            plt.subplot(2, 2, i+1)
            plt.title(names[0][i])
            for j in range(labels[i].shape[0]):
                if y is None:
                    y = labels[i][j, :, :]
                else:
                    y = np.concatenate((y, labels[i][j, :, :]), axis=0)
            for j in range(self._params['nb_classes']):
                plt.plot(x, y[:, j], label=str(j))
            # plt.legend(loc='best')

        for i in range(2):
            x = np.arange(predicted[i].shape[0] * predicted[i].shape[1])
            y = None
            plt.subplot(2, 2, i+3)
            plt.title(names[1][i])
            for j in range(predicted[i].shape[0]):
                if y is None:
                    y = predicted[i][j, :, :]
                else:
                    y = np.concatenate((y, predicted[i][j, :, :]), axis=0)
            for j in range(self._params['nb_classes']):
                plt.plot(x, y[:, j], label=str(j))
            # plt.legend(loc='best')
        #plt.legend(loc='best')
        plt.show()

